/*
 * Copyright (c) The original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package id.xbind.dilm.support;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import id.scanpath.ClasspathScanner;
import id.scanpath.ScanScope;
import id.xbind.dilm.ConfigurationProvider;
import id.xbind.dilm.XBindException;
import id.xbind.dilm.GlobalLifecycleInterceptor;
import id.xbind.dilm.Injector;
import id.xbind.dilm.LifecycleInterceptor;
import id.xbind.dilm.Locator;
import id.xbind.dilm.MethodInterceptor;
import id.xbind.dilm.XBind;
import id.xbind.dilm.support.binder.Binder;

/**
 * @author indroneel
 *
 */

public class XBindImpl extends XBind {

	private static final Logger LOGGER = Logger.getLogger(XBindImpl.class.getName());

	private ClasspathScanner            scanner;
	private ScanScope                   scope;
	private List<ConfigurationProvider> configProviders;
	private LocatorImpl                 locator;
	private InjectorImpl                injector;
	private EventHandlerImpl            evtHandler;
	private BinderCreator               bindCreator;
	private BinderOrdering              bindOrder;
	private boolean                     startFlag;

	public XBindImpl() {
		scanner = new ClasspathScanner();
		locator = new LocatorImpl();
		evtHandler = new EventHandlerImpl(locator);
		configProviders = new ArrayList<>();
		injector = new InjectorImpl(locator, configProviders);

		bindCreator = new BinderCreator();
		bindCreator.setClasspathScanner(scanner);
		bindCreator.setLocator(locator);
		bindCreator.setInjector(injector);
		bindCreator.setEventHandler(evtHandler);
		bindCreator.setConfigurationProviders(configProviders);

		bindOrder = new BinderOrdering();
		bindOrder.setLocator(locator);
		startFlag = false;
	}

	////////////////////////////////////////////////////////////////////////////
	// Methods of base class CrossBinder

	@Override
	public XBind scanClasses(ScanScope ss) {
		scope = ss;
		return this;
	}

	@Override
	public XBind configure(ConfigurationProvider provider) {
		configProviders.add(provider);
		return this;
	}

	@Override
	public XBind classLoader(ClassLoader cl) {
		scanner.setClassLoader(cl);
		bindCreator.setClassLoader(cl);
		return this;
	}

	@Override
	public XBind addInterceptor(MethodInterceptor mi) {
		bindCreator.addInterceptor(mi);
		return this;
	}

	@Override
	public XBind addInterceptor(LifecycleInterceptor li) {
		bindCreator.addInterceptor(li);
		return this;
	}

	@Override
	public XBind addLocator(Locator loc) {
		locator.chain(loc);
		return this;
	}

	@Override
	public XBind start() throws XBindException {
		scanner.load(scope);

		bindCreator.loadScanned();
		locator.dumpState(); //for debug purposes

		CircularDependencies circDep = new CircularDependencies();
		circDep.setLocator(locator);
		circDep.check();

		bindOrder.resolve();

		for(Binder binder : bindOrder.getStage1Binders()) {
			binder.start();
		}
		LOGGER.fine("stage1 binders started");

		//activate the event handler to start handling events from this point onwards.
		evtHandler.getReady();

		for(Binder binder : bindOrder.getStage2Binders()) {
			binder.start();
		}
		LOGGER.fine("stage2 binders started");

		for(Binder binder : locator.getGlobalLifecycleInterceptors()) {
			GlobalLifecycleInterceptor glci = binder.getInstance(GlobalLifecycleInterceptor.class);
			if(glci != null) {
				glci.afterStart();
			}
		}
		startFlag = true;
		return this;
	}

	@Override
	public void stop() {

		for(Binder binder : locator.getGlobalLifecycleInterceptors()) {
			GlobalLifecycleInterceptor glci = binder.getInstance(GlobalLifecycleInterceptor.class);
			if(glci != null) {
				glci.beforeStop();
			}
		}

		for(Binder binder : bindOrder.getStage2Binders()) {
			binder.stop();
		}

		for(Binder binder : bindOrder.getStage1Binders()) {
			binder.stop();
		}
		startFlag = false;
	}

	@Override
	public boolean isStarted() {
		return startFlag;
	}

	@Override
	public Injector injector() {
		return injector;
	}

	@Override
	public Locator locator() {
		return locator;
	}
}
