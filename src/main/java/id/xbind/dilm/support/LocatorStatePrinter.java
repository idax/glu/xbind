/*
 * Copyright (c) The original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package id.xbind.dilm.support;

import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import id.xbind.dilm.Locator;
import id.xbind.dilm.support.binder.Binder;

/**
 * @author indroneel
 *
 */

public class LocatorStatePrinter {

	private static final Logger _L = Logger.getLogger(Locator.class.getName());

	public void printTypeToBinder(Map<Class<?>, Set<Binder>> typeToBinderMap) {
		_L.info("crossbinder: binder to type mapping");
		_L.info("........................................................................");
		for(Class<?> type : typeToBinderMap.keySet()) {
			if(type.getPackage() != null) {
				_L.info(String.format("%s (%s)",
						type.getSimpleName(), type.getPackage().getName()));
			}
			else {
				_L.info(type.getSimpleName());
			}
			Set<Binder> binders = typeToBinderMap.get(type);
			int maxw = 4;
			for(Binder binder : binders) {
				if(binder.getName() != null) {
					maxw = Math.max(maxw, binder.getName().length());
				}
			}
			String fmt = "-> %-" + maxw + "s = %s";
			for(Binder binder : binders) {
				if(binder.getName() != null && binder.getName().trim().length() > 0) {
					_L.info(String.format(fmt, binder.getName(), binder.toString()));
				}
				else {
					_L.info(String.format("-> %s", binder.toString()));
				}
			}
		}
		_L.info("........................................................................");
	}

	private String abbrFqcn(String fqcn) {
		StringBuilder sb = new StringBuilder();
		String[] parts = fqcn.split("\\.");
		for(int i = 0; i < parts.length - 1; i++) {
			sb.append(parts[i].charAt(0)).append('.');
		}
		sb.append(parts[parts.length - 1]);
		return sb.toString();
	}
}
