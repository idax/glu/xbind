/*
 * Copyright (c) The original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package id.xbind.dilm.support.binder.types;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import id.xbind.dilm.XBindException;
import id.xbind.dilm.Disposable;
import id.xbind.dilm.Dispose;

/**
 * @author indroneel
 *
 */

public class DisposeProcessor {

	private static final Logger LOGGER = Logger.getLogger(DisposeProcessor.class.getName());

	public void resolve(Class<?> targetCls) {
		List<Method> disposeMthds = findDisposeMethods(targetCls);
		if(Disposable.class.isAssignableFrom(targetCls) && !disposeMthds.isEmpty()) {
			LOGGER.warning(String.format("conflict_dispose = %s (both disposable and has dispose method)",
					targetCls.getName()));
			throw new XBindException("conflict in disposal. Class is both disposable and has dispose methods.");
		}
		if(disposeMthds.size() > 1) {
			LOGGER.warning(String.format("conflict_dispose = %s (too many dispose methods)", targetCls.getName()));
			throw new XBindException("conflict in disposal. Too many dispose methods.");
		}
	}

	public void execute(Object target) {
		if(target instanceof Disposable) {
			((Disposable) target).dispose();
		}
		else {
			List<Method> disposeMthds = findDisposeMethods(target.getClass());
			if(!disposeMthds.isEmpty()) {
				try {
					disposeMthds.get(0).invoke(target);
				}
				catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException exep) {
					throw new XBindException("error executing dispose method",
							exep.getCause() != null ? exep.getCause() : exep);
				}
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	// Helper methods

	private List<Method> findDisposeMethods(Class<?> cls) {
		Method[] methods = cls.getMethods();
		ArrayList<Method> result = new ArrayList<>(methods.length);
		for(Method method : methods) {
			if(method.getAnnotation(Dispose.class) != null) {
				Class<?>[] paramTypes = method.getParameterTypes();
				if(paramTypes.length == 0 && method.getReturnType() == Void.TYPE) {
					result.add(method);
				}
				else {
					LOGGER.warning(String.format("bad_dispose_method = %s (should have zero args and no return type)",
							method.getName()));
				}
			}
		}
		return result;
	}
}
