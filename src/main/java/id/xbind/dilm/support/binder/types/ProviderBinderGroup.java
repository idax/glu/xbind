/*
 * Copyright (c) The original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package id.xbind.dilm.support.binder.types;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import id.xbind.dilm.XBindException;
import id.xbind.dilm.InjectorAware;
import id.xbind.dilm.LocatorAware;
import id.xbind.dilm.Provides;
import id.xbind.dilm.support.binder.Binder;
import id.xbind.dilm.support.binder.BinderContext;
import id.xbind.dilm.support.binder.Dependency;

/**
 * @author indroneel
 *
 */

class ProviderBinderGroup {

	private Class<?>            implCls;
	private BinderContext       binderCtxt;
	private Set<Dependency>     dependencies;
	private List<Binder>        binders;
	private Object              provider;
	private boolean             started;

	public ProviderBinderGroup(Class<?> implCls, BinderContext ctxt) {
		this.implCls = implCls;
		binderCtxt = ctxt;
		dependencies = new InjectProcessor().extractDependencies(implCls);
		new InitProcessor().resolve(implCls);
		new DisposeProcessor().resolve(implCls);
		createBinders();
	}

	public Set<Dependency> getDependencies() {
		return dependencies;
	}

	public synchronized void start() {
		if(started) {
			return;
		}

		// Create new provider instance via invocation of default constructor.
		try {
			provider = implCls.newInstance();
		}
		catch (InstantiationException | IllegalAccessException exep) {
			throw new XBindException("unable to instantiate provider", exep);
		}

		// Process annotations and inject configuration.
		new ConfigProcessor(binderCtxt.getConfigurationProviders()).configure(provider);

		// Do injection
		if(provider instanceof LocatorAware) {
			((LocatorAware) provider).setLocator(binderCtxt.getLocator());
		}
		if(provider instanceof InjectorAware) {
			((InjectorAware) provider).setInjector(binderCtxt.getInjector());
		}
		new InjectProcessor().injectDependencies(provider, binderCtxt.getLocator());

		// Invoke init method on target instance
		new InitProcessor().execute(provider);

		started = true;
	}

	public synchronized void stop() {
		if(!started) {
			return;
		}
		new DisposeProcessor().execute(provider);
		started = false;
	}

	public Object getProvider() {
		return provider;
	}

	public List<Binder> getBinders() {
		return binders;
	}

	public BinderContext getBinderContext() {
		return binderCtxt;
	}

	public String toString() {
		return implCls.getName();
	}

	////////////////////////////////////////////////////////////////////////////
	// Helper methods

	private void createBinders() {
		List<Class<?>> uniqueCls = new ArrayList<>();
		binders = new ArrayList<>();

		Method[] methods = implCls.getMethods();
		for(Method method : methods) {
			if(method.getAnnotation(Provides.class) == null) {
				//method must be annotated with @Provides
				continue;
			}
			if(method.getParameterCount() != 0) {
				//method should not take any arguments
				continue;
			}
			Class<?> retype = method.getReturnType();
			if(!retype.isInterface()) {
				// the declared method return type must be an interface
				continue;
			}
			if(uniqueCls.contains(retype)) {
				//TODO: Log error
				throw new XBindException("duplicate methods providing the same type within the same provider");
			}

			Provides ann = method.getAnnotation(Provides.class);
			String name = ann.value().trim();
			binders.add(new ProviderBinder(this, name, method));
			uniqueCls.add(retype);
		}
	}
}
