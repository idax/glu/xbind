/*
 * Copyright (c) The original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package id.xbind.dilm.support.binder.types;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import id.xbind.dilm.Bindable;
import id.xbind.dilm.XBindException;
import id.xbind.dilm.InjectorAware;
import id.xbind.dilm.Lazy;
import id.xbind.dilm.LocatorAware;
import id.xbind.dilm.NonBindable;
import id.xbind.dilm.Prototype;
import id.xbind.dilm.support.binder.Binder;
import id.xbind.dilm.support.binder.BinderContext;
import id.xbind.dilm.support.binder.Dependency;

/**
 * @author indroneel
 *
 */

public class PrototypeBinder implements Binder {

	private static final Logger LOGGER = Logger.getLogger(PrototypeBinder.class.getName());

	private Class<?>            implCls;
	private BinderContext       binderCtxt;
	private Set<Dependency>     dependencies;
	private Set<Class<?>>       bindToList;
	private Map<Method, Method> methodMap;

	PrototypeBinder(Class<?> implCls, BinderContext ctxt) {
		this.implCls = implCls;
		binderCtxt = ctxt;

		Set<Class<?>> ifaces = new HashSet<>();
		retrieveInterfaces(implCls, ifaces);
		Set<Class<?>> bindables = retrieveBindables(ifaces);
		if(bindables.isEmpty()) {
			bindToList = ifaces;
		}
		else {
			bindToList = bindables;
		}
		dependencies = new InjectProcessor().extractDependencies(implCls);
		new InitProcessor().resolve(implCls);
	}

	////////////////////////////////////////////////////////////////////////////
	// Methods of interface Binder

	@Override
	public String getName() {
		Prototype ann = implCls.getAnnotation(Prototype.class);
		return ann.value().trim();
	}

	@Override
	public Set<Class<?>> getInterfaceTypes() {
		return bindToList;
	}

	@Override
	public Set<Dependency> getDependencies() {
		return dependencies;
	}

	@Override
	public <T> T getInstance(Class<T> type) {
		if(type == null) {
			LOGGER.fine("lookup type cannot be null");
			return null;
		}
		if(!bindToList.contains(type)) {
			LOGGER.fine(String.format("instance not found type = %s", type.getName()));
			return null;
		}

		Lazy ann = implCls.getAnnotation(Lazy.class);

		Object protoProxy = Proxy.newProxyInstance(
			binderCtxt.getClassLoader(),
			new Class<?>[]{type},
			new PrototypeInvocationHandler(ann != null));
		return type.cast(protoProxy);
	}

	@Override
	public void start() {
		methodMap = new HashMap<>();
		for(Class<?> iface : bindToList) {
			Method[] methods = iface.getMethods();
			for(Method mthd : methods) {
				try {
					Method outMthd = implCls.getMethod(mthd.getName(), mthd.getParameterTypes());
					methodMap.put(mthd, outMthd);
				}
				catch(Exception exep) {
					LOGGER.log(Level.WARNING, "unable to map method " + mthd.getName(), exep);
				}
			}
		}
	}

	@Override
	public void stop() {
		// TODO Auto-generated method stub
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder("prototype: ")
			.append(implCls.getName());
		return builder.toString();
	}

	////////////////////////////////////////////////////////////////////////////
	// Methods of base class Object

	@Override
	public int hashCode() {
		if(implCls != null) {
			return implCls.hashCode();
		}
		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if(obj instanceof PrototypeBinder) {
			PrototypeBinder binder = (PrototypeBinder) obj;
			return (binder.implCls != null && implCls != null && binder.implCls.equals(implCls));
		}
		return false;
	}

	////////////////////////////////////////////////////////////////////////////
	// Helper methods

	private void retrieveInterfaces(Class<?> cls, Set<Class<?>> interfaces) {
		if(!cls.isInterface()) {
			Class<?> superCls = cls.getSuperclass();
			if(superCls != null) {
				retrieveInterfaces(superCls, interfaces);
			}
		}

		Class<?>[] ifaces = cls.getInterfaces();
		if(ifaces.length == 0) {
			return;
		}
		for(Class<?> iface : ifaces) {
			if(iface.getAnnotation(NonBindable.class) == null) {
				interfaces.add(iface);
			}
		}
		for(Class<?> iface : ifaces) {
			retrieveInterfaces(iface, interfaces);
		}
	}

	private Set<Class<?>> retrieveBindables(Set<Class<?>> interfaces) {
		Set<Class<?>> result = new HashSet<Class<?>>();
		for(Class<?> iface : interfaces) {
			if(iface.getAnnotation(Bindable.class) != null) {
				result.add(iface);
			}
		}
		return result;
	}

	private synchronized Object createInstance(Object oldProto) {
		if(oldProto != null) {
			return oldProto;
		}

		Object proto = null;
		try {
			proto = implCls.newInstance();
		}
		catch (InstantiationException | IllegalAccessException exep) {
			throw new XBindException("unable to instantiate prototype " + this, exep);
		}

		// Notify event processors that object has been created.
		binderCtxt.getEventHandler().instanceCreated(proto);

		// Process annotations and inject configuration.
		new ConfigProcessor(binderCtxt.getConfigurationProviders()).configure(proto);

		// Do injection
		if(proto instanceof LocatorAware) {
			((LocatorAware) proto).setLocator(binderCtxt.getLocator());
		}
		if(proto instanceof InjectorAware) {
			((InjectorAware) proto).setInjector(binderCtxt.getInjector());
		}
		new InjectProcessor().injectDependencies(proto, binderCtxt.getLocator());

		// Notify event processors that object has been injected.
		binderCtxt.getEventHandler().instanceInjected(proto);

		// Invoke init method on target instance
		new InitProcessor().execute(proto);

		// Notify event processors that object has been initialized.
		binderCtxt.getEventHandler().instanceInitialized(proto);

		return proto;
	}

	////////////////////////////////////////////////////////////////////////////
	// Inner class that implements the InvocationHandler

	private class PrototypeInvocationHandler implements InvocationHandler {

		private Object proto;

		public PrototypeInvocationHandler(boolean lazy) {
			if(!lazy) {
				proto = createInstance(proto);
			}
		}

		@Override
		public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

			if(proto == null) {
				proto = createInstance(proto);
			}
			//Method outMthd = implCls.getMethod(method.getName(), method.getParameterTypes());
			Method outMthd = methodMap.get(method);
			if(outMthd == null) {
				throw new RuntimeException("unable to map method " + method.getName());
			}

			binderCtxt.getEventHandler().beforeMethod(proto, outMthd, args);
			Object retVal = null;
			try {
				retVal = binderCtxt.getEventHandler().wrapMethod(proto, outMthd, args);
			}
			catch(InvocationTargetException exep) {
				binderCtxt.getEventHandler().afterMethodFail(proto, outMthd, exep.getCause());
				throw exep.getCause();
			}
			binderCtxt.getEventHandler().afterMethodSuccess(proto, outMthd, retVal);
			return retVal;
		}
	}
}
