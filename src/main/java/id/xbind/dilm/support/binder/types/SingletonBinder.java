/*
 * Copyright (c) The original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package id.xbind.dilm.support.binder.types;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import id.xbind.dilm.Bindable;
import id.xbind.dilm.XBindException;
import id.xbind.dilm.InjectorAware;
import id.xbind.dilm.Lazy;
import id.xbind.dilm.LocatorAware;
import id.xbind.dilm.NonBindable;
import id.xbind.dilm.Singleton;
import id.xbind.dilm.support.binder.Binder;
import id.xbind.dilm.support.binder.BinderContext;
import id.xbind.dilm.support.binder.Dependency;

/**
 * @author indroneel
 *
 */

public class SingletonBinder implements Binder {

	private static final Logger LOGGER = Logger.getLogger(SingletonBinder.class.getName());

	private Class<?>        implCls;
	private BinderContext   binderCtxt;
	private Set<Dependency> dependencies;
	private Set<Class<?>>   bindToList;
	private Object          singleton;
	private Object          proxySingleton;

	SingletonBinder(Class<?> implCls, BinderContext ctxt) {
		this.implCls = implCls;
		binderCtxt = ctxt;

		Set<Class<?>> ifaces = new HashSet<>();
		retrieveInterfaces(implCls, ifaces);
		Set<Class<?>> bindables = retrieveBindables(ifaces);
		if(bindables.isEmpty()) {
			bindToList = ifaces;
		}
		else {
			bindToList = bindables;
		}
		dependencies = new InjectProcessor().extractDependencies(implCls);
		new InitProcessor().resolve(implCls);
		new DisposeProcessor().resolve(implCls);
	}

	////////////////////////////////////////////////////////////////////////////
	// Methods of interface Binder

	@Override
	public String getName() {
		Singleton ann = implCls.getAnnotation(Singleton.class);
		return ann.value().trim();
	}

	@Override
	public Set<Class<?>> getInterfaceTypes() {
		return bindToList;
	}

	@Override
	public Set<Dependency> getDependencies() {
		return dependencies;
	}

	@Override
	public <T> T getInstance(Class<T> type) {
		if(type == null) {
			LOGGER.fine("lookup type cannot be null");
			return null;
		}
		if(!bindToList.contains(type)) {
			LOGGER.fine(String.format("instance not found type = %s", type.getName()));
			return null;
		}
		return type.cast(proxySingleton);
	}

	@Override
	public void start() {
		LOGGER.fine(String.format("singleton = {%s}, starting", implCls.getName()));
		boolean shadow = getInterfaceTypes().isEmpty();
		Lazy ann = implCls.getAnnotation(Lazy.class);

		if(ann == null || shadow) {
			// keep the instance ready in case not lazyloading or is a shadow singleton.
			createInstance();
		}
		//and create the proxy instance for the real instance
		if(!shadow) {
			createProxyInstance();
		}
	}

	@Override
	public void stop() {
		if(singleton != null) {
			new DisposeProcessor().execute(singleton);
		}
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder("singleton: ")
			.append(implCls.getName());
		return builder.toString();
	}

	////////////////////////////////////////////////////////////////////////////
	// Methods of base class Object

	@Override
	public int hashCode() {
		if(implCls != null) {
			return implCls.hashCode();
		}
		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if(obj instanceof SingletonBinder) {
			SingletonBinder binder = (SingletonBinder) obj;
			return (binder.implCls != null && implCls != null && binder.implCls.equals(implCls));
		}
		return false;
	}

	////////////////////////////////////////////////////////////////////////////
	// Helper methods

	private void retrieveInterfaces(Class<?> cls, Set<Class<?>> interfaces) {
		if(!cls.isInterface()) {
			Class<?> superCls = cls.getSuperclass();
			if(superCls != null) {
				retrieveInterfaces(superCls, interfaces);
			}
		}

		Class<?>[] ifaces = cls.getInterfaces();
		if(ifaces.length == 0) {
			return;
		}
		for(Class<?> iface : ifaces) {
			if(iface.getAnnotation(NonBindable.class) == null) {
				interfaces.add(iface);
			}
		}
		for(Class<?> iface : ifaces) {
			retrieveInterfaces(iface, interfaces);
		}
	}

	private Set<Class<?>> retrieveBindables(Set<Class<?>> interfaces) {
		Set<Class<?>> result = new HashSet<Class<?>>();
		for(Class<?> iface : interfaces) {
			if(iface.getAnnotation(Bindable.class) != null) {
				result.add(iface);
			}
		}
		return result;
	}

	private synchronized void createInstance() throws XBindException {
		if(singleton != null) {
			//the instance might have been created on some other thread, in case of lazy load.
			return;
		}
		Object single0 = null;
		try {
			single0 = implCls.newInstance();
		}
		catch (InstantiationException | IllegalAccessException exep) {
			throw new XBindException("unable to instantiate singeton " + this, exep);
		}

		// Notify event processors that object has been created.
		binderCtxt.getEventHandler().instanceCreated(single0);

		// Process annotations and inject configuration.
		new ConfigProcessor(binderCtxt.getConfigurationProviders()).configure(single0);

		// Do injection
		if(single0 instanceof LocatorAware) {
			((LocatorAware) single0).setLocator(binderCtxt.getLocator());
		}
		if(single0 instanceof InjectorAware) {
			((InjectorAware) single0).setInjector(binderCtxt.getInjector());
		}
		new InjectProcessor().injectDependencies(single0, binderCtxt.getLocator());

		// Notify event processors that object has been injected.
		binderCtxt.getEventHandler().instanceInjected(single0);

		// Invoke init method on target instance
		new InitProcessor().execute(single0);

		// Notify event processors that object has been initialized.
		binderCtxt.getEventHandler().instanceInitialized(single0);

		//and assign the local instance to the actual singleton.
		singleton = single0;
	}

	private void createProxyInstance() {
		Class<?>[] ifaces = bindToList.toArray(new Class[bindToList.size()]);
		proxySingleton = Proxy.newProxyInstance(
				binderCtxt.getClassLoader(),
				ifaces,
				new SingletonInvocationHandler(ifaces));
	}

	////////////////////////////////////////////////////////////////////////////
	// Inner class that implements the InvocationHandler

	private class SingletonInvocationHandler implements InvocationHandler {

		private Map<Method, Method> methodMap;

		public SingletonInvocationHandler(Class<?>[] ifaces) {
			methodMap = new HashMap<>();
			for(Class<?> iface : ifaces) {
				Method[] methods = iface.getMethods();
				for(Method mthd : methods) {
					try {
						Method outMthd = implCls.getMethod(mthd.getName(), mthd.getParameterTypes());
						methodMap.put(mthd, outMthd);
					}
					catch(Exception exep) {
						LOGGER.log(Level.WARNING, "unable to map method " + mthd.getName(), exep);
					}
				}
			}
		}

		@Override
		public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
			if(singleton == null) {
				createInstance();
			}
			Method outMthd = methodMap.get(method);
			if(outMthd == null) {
				throw new RuntimeException("unable to map method " + method.getName());
			}

			Object retVal = null;
			binderCtxt.getEventHandler().beforeMethod(singleton, outMthd, args);
			try {
				retVal = binderCtxt.getEventHandler().wrapMethod(singleton, outMthd, args);
			}
			catch(InvocationTargetException exep) {
				binderCtxt.getEventHandler().afterMethodFail(singleton, outMthd, exep.getCause());
				throw exep.getCause();
			}
			binderCtxt.getEventHandler().afterMethodSuccess(singleton, outMthd, retVal);

			return retVal;
		}
	}
}
