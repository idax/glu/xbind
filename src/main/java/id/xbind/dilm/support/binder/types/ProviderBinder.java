/*
 * Copyright (c) The original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package id.xbind.dilm.support.binder.types;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import id.xbind.dilm.XBindException;
import id.xbind.dilm.support.binder.Binder;
import id.xbind.dilm.support.binder.Dependency;

/**
 * @author indroneel
 *
 */

public class ProviderBinder implements Binder {

	private static final Logger LOGGER = Logger.getLogger(ProviderBinder.class.getName());

	private String              name;
	private ProviderBinderGroup binderGroup;
	private Method              provMthd;

	ProviderBinder(ProviderBinderGroup group, String name, Method mthd) {
		this.name = name;
		binderGroup = group;
		provMthd = mthd;
	}

	////////////////////////////////////////////////////////////////////////////
	// Methods of interface Binder

	@Override
	public String getName() {
		return name;
	}

	@Override
	public Set<Class<?>> getInterfaceTypes() {
		HashSet<Class<?>> reset = new HashSet<>();
		reset.add(provMthd.getReturnType());
		return reset;
	}

	@Override
	public Set<Dependency> getDependencies() {
		return binderGroup.getDependencies();
	}

	@Override
	public <T> T getInstance(Class<T> type) {
		if(type == null) {
			LOGGER.fine("lookup type cannot be null");
			return null;
		}

		try {
			Object result = provMthd.invoke(binderGroup.getProvider());
			Object protoProxy = createProxyInstance(result);
			return type.cast(protoProxy);
		}
		catch (IllegalAccessException
				| IllegalArgumentException | InvocationTargetException exep) {
			LOGGER.warning(String.format("unable to create provided instance: provider = %s, method = %s",
				provMthd.getDeclaringClass().getName(), provMthd.getName()));
			throw new XBindException(
				"unable to create provided instance",
				(exep.getCause() != null)? exep.getCause() : exep);
		}
	}

	@Override
	public void start() {
		binderGroup.start();
	}

	@Override
	public void stop() {
		binderGroup.stop();
	}

	////////////////////////////////////////////////////////////////////////////
	// Methods of base class Object

	@Override
	public int hashCode() {
		return provMthd.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if(obj instanceof ProviderBinder) {
			ProviderBinder binder = (ProviderBinder) obj;
			return binder.provMthd.equals(provMthd);
		}
		return false;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder("provider: ")
			.append(binderGroup.toString());
		return builder.toString();
	}

	private Object createProxyInstance(Object prototype) {
		return Proxy.newProxyInstance(
				binderGroup.getBinderContext().getClassLoader(),
				new Class[]{provMthd.getReturnType()},
				new ProvidedInvocationHandler(prototype, provMthd.getReturnType()));
	}

	////////////////////////////////////////////////////////////////////////////
	// Inner class that implements the InvocationHandler

	private class ProvidedInvocationHandler implements InvocationHandler {

		private Object              provided;
		private Map<Method, Method> methodMap;

		public ProvidedInvocationHandler(Object provided, Class<?> rtnType) {
			this.provided = provided;
			methodMap = new HashMap<>();
			Method[] methods = rtnType.getMethods();
			for(Method mthd : methods) {
				try {
					Method outMthd = provided.getClass().getMethod(mthd.getName(), mthd.getParameterTypes());
					methodMap.put(mthd, outMthd);
				}
				catch(Exception exep) {
					LOGGER.log(Level.WARNING, "unable to map method " + mthd.getName(), exep);
				}
			}
		}

		@Override
		public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
			//Method outMthd = provided.getClass().getMethod(method.getName(), method.getParameterTypes());
			Method outMthd = methodMap.get(method);
			if(outMthd == null) {
				throw new RuntimeException("unable to map method " + method.getName());
			}

			binderGroup.getBinderContext().getEventHandler()
					.beforeMethod(provided, outMthd, args);
			Object retVal = null;
			try {
				retVal = binderGroup.getBinderContext().getEventHandler()
						.wrapMethod(provided, outMthd, args);
			}
			catch(InvocationTargetException exep) {
				binderGroup.getBinderContext().getEventHandler()
						.afterMethodFail(provided, outMthd, exep.getCause());
				throw exep.getCause();
			}
			binderGroup.getBinderContext()
					.getEventHandler().afterMethodSuccess(provided, outMthd, retVal);
			return retVal;
		}
	}
}
