/*
 * Copyright (c) The original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package id.xbind.dilm;

/**
 * Allows insertion of custom logic at specific points in the overall lifecycle of singletons and
 * protptypes managed by xBind. The intention is to allow for common operations to be performed as
 * and when entities get created and destroyed.
 * <p>
 *
 * Multiple such interceptors (one instance per type) may be active for a given xBind instance. All
 * interceptors get an equal opportunity to execute their logic on a managed entity against a given
 * lifecycle stage. The order in which these interceptors are invoked is determined by xBind. The
 * ordering is fixed for a given xBind instance, but may vary unpredictably across xBind lifecycles.
 * <p>
 *
 * A lifecycle interceptor can be instantiated by xBind after scanning the classpath, in which case
 * it is injected with dependencies and configuration data like any other managed entity. Such
 * interceptors may also be created externally and then associated with a xBind via the
 * {@link XBind#addInterceptor(LifecycleInterceptor)} method.
 *
 * @author indroneel
 */

public interface LifecycleInterceptor {

/**
 * This method gets invoked after a singleton or prototype has been created by xBind, and prior to
 * any other dependency injection or execution of initialization routines.
 *
 * @param	target the singleton or prototype created by xBind
 */

	void afterCreation(Object target);

/**
 * This method gets invoked after a singleton or prototype has been created and injected with all
 * available dependencies by xBind.
 *
 * @param	target the singleton or prototype managed by xBind
 */

	void afterInjection(Object target);

/**
 * This method gets invoked after a singleton or prototype has been created, injected with all
 * available dependencies and execution of init-method by xBind.
 *
 * @param	target the singleton or prototype managed by xBind
 */

	void afterInitialization(Object target);

/**
 * This method gets invoked at a stage when a singleton is about to be destroyed and removed by
 * xBind. Since a singleton's end-of-life is aligned with that of the containing xBind, this method
 * gets invoked at the time of a clean xBind shutdown.
 *
 * @param	target the singleton managed by xBind
 */

	void beforeDisposal(Object target);

/**
 * This method gets invoked at a stage when a singleton is destroyed and removed from xBind. Since a
 * singleton's end-of-life is aligned with that of the containing xBind, this method gets invoked at
 * the time of a clean xBind shutdown.
 *
 * @param	target the singleton or prototype managed by xBind
 */

	void afterDisposal(Object target);
}
