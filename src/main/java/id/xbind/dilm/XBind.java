/*
 * Copyright (c) The original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package id.xbind.dilm;

import java.util.logging.Logger;

import id.scanpath.ScanScope;

/**
 * This class forms the entry point to the xBind container for singletons, prototypes and providers.
 *
 * @author indroneel
 */

public abstract class XBind {

	private static final Logger LOGGER = Logger.getLogger(XBind.class.getName());

	protected XBind() {
		//NOOP
	}

	public static XBind create() throws XBindException {
		LOGGER.info("xBind: Lightweight Dependency Injection and Lifecycle Management");

		String clsName = XBind.class.getPackage().getName() + ".support.XBindImpl";
		LOGGER.fine(String.format("xbind_engine = %s", clsName));

		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		if(cl == null) {
			cl = ClassLoader.getSystemClassLoader();
		}

		try {
			return (XBind) cl.loadClass(clsName).newInstance();
		}
		catch (ClassNotFoundException exep) {
			throw new XBindException("xBind implementation class not found " + clsName, exep);
		}
		catch (InstantiationException | IllegalAccessException exep) {
			throw new XBindException("unable to create xBind instance " + clsName, exep);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	// Methods to be implemented in concrete classes

/**
 * Look for managed entities and interceptors from the classpath under the given scope.
 * <p>
 *
 * @param   scope limits the classpath entires to be visited in order to identify managed entities
 *          and interceptors.
 * @return	this instance of xBind.
 */

	public abstract XBind scanClasses(ScanScope scope);

	public abstract XBind configure(ConfigurationProvider provider);

	public abstract XBind classLoader(ClassLoader cl);

/**
 * Lets you associate a method interceptor, whose lifecycle is not managed by xBind.
 *
 * @param	li the method interceptor
 * @return	this instance of xBind
 */

	public abstract XBind addInterceptor(MethodInterceptor mi);

/**
 * Lets you associate a lifecycle interceptor, whose lifecycle is not managed by xBind.
 *
 * @param	li the lifecycle interceptor
 * @return	this instance of xBind
 */

	public abstract XBind addInterceptor(LifecycleInterceptor li);

/**
 * Lets you add a custom locator (e.g. one that wraps around a Spring or a Guice container), leading
 * to chaining of DI containers. These locators will be referenced for entities, only when the same
 * cannot be resolved against this instance of xBind.
 *
 * @param	locator a custom locator instance.
 * @return	this instance of xBind.
 */

	public abstract XBind addLocator(Locator locator);

	public abstract XBind start() throws XBindException;

	public abstract void stop() throws XBindException;

	public abstract boolean isStarted();


/**
 * Retrieves the injector facet that is associated with this xBind. The injector is used to populate
 * external objects with entities managed by this xBind.
 * <p>
 *
 * @return	the injector facet for this xBind.
 */

	public abstract Injector injector();

/**
 * Retrieves the locator facet that is associated with this xBind. The locator is used to access
 * entities managed by this xBind from an external scope.
 * <p>
 *
 * @return	the locator facet for this xBind.
 */

	public abstract Locator locator();

	//public abstract Locator limitedLocator();
}
