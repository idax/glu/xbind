/*
 * Copyright (c) The original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package id.xbind.dilm;

/**
 * Exception that is thrown during startup, shutdown and other xBind-specific operations (e.g.
 * interception, injection, etc.)
 *
 * @author indroneel
 */

@SuppressWarnings("serial")
public class XBindException extends RuntimeException {

	public XBindException() {
		super();
	}

	public XBindException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public XBindException(String message, Throwable cause) {
		super(message, cause);
	}

	public XBindException(String message) {
		super(message);
	}

	public XBindException(Throwable cause) {
		super(cause);
	}
}
